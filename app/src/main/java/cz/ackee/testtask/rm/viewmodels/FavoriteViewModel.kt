package cz.ackee.testtask.rm.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.ackee.testtask.app_domain.feature.character.GetFavoriteCharactersUseCase
import cz.ackee.testtask.app_domain.model.Character
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/19/2024
*/
class FavoriteViewModel(private val getFavoriteCharactersUseCase: GetFavoriteCharactersUseCase) :
    ViewModel() {

    private val _favorites = MutableStateFlow<List<Character>>(listOf())
    val favorites = _favorites

    fun getFavorites() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = getFavoriteCharactersUseCase()
            if (result.isSuccess()) {
                val character = result.getOrNull()
                if (character != null) {
                    _favorites.emit(character)
                }
            }
        }
    }
}