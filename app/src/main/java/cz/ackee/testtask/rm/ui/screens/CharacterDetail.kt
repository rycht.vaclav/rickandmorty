package cz.ackee.testtask.rm.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.R
import cz.ackee.testtask.rm.ui.components.item.CharacterDetailItem
import cz.ackee.testtask.rm.ui.components.topappbar.CharacterDetailTopAppBar
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette
import cz.ackee.testtask.rm.viewmodels.CharacterDetailViewModel

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@Composable
fun CharacterDetail(navController: NavController, viewModel: CharacterDetailViewModel) {


    val detail = viewModel.detail.collectAsState().value

    Scaffold(topBar = {
        CharacterDetailTopAppBar(
            viewModel = viewModel,
            navController = navController,
            detail = detail
        )
    }) {
        detail?.let { characterDetail ->
            Column(
                modifier = Modifier
                    .padding(it)
                    .shadow(elevation = 4.dp, shape = RoundedCornerShape(16.dp))
                    .background(LocalCustomColorsPalette.current.primary)
                    .clip(shape = RoundedCornerShape(16.dp))
            ) {
                Row {
                    AsyncImage(
                        modifier = Modifier
                            .padding(16.dp)
                            .size(144.dp)
                            .clip(RoundedCornerShape(8.dp)),
                        model = characterDetail.image,
                        contentDescription = characterDetail.name,
                        placeholder = painterResource(id = R.drawable.latest),
                        contentScale = ContentScale.Crop
                    )
                    Column {
                        Text(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 16.dp, end = 16.dp),
                            text = Constants.NAME,
                            color = LocalCustomColorsPalette.current.secondaryText
                        )
                        Text(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 16.dp, end = 16.dp),
                            text = characterDetail.name ,
                            color = LocalCustomColorsPalette.current.mainText,
                            fontWeight = FontWeight.Bold,
                            fontSize = 20.sp
                        )
                    }
                }
                HorizontalDivider(
                    modifier = Modifier.fillMaxWidth(),
                    color = LocalCustomColorsPalette.current.divider,
                    thickness = 2.dp
                )
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState())
                ) {
                    CharacterDetailItem(
                        title = Constants.STATUS,
                        text = characterDetail.status
                    )
                    CharacterDetailItem(
                        title = Constants.SPECIES,
                        text = characterDetail.species
                    )
                    CharacterDetailItem(title = Constants.TYPE, text = characterDetail.type )
                    CharacterDetailItem(
                        title = Constants.GENDER,
                        text = characterDetail.gender
                    )
                    CharacterDetailItem(
                        title = Constants.ORIGIN,
                        text = characterDetail.origin
                    )
                    CharacterDetailItem(
                        title = Constants.LOCATION,
                        text = characterDetail.location
                    )
                    Spacer(modifier = Modifier.size(height = 16.dp, width = 0.dp))
                }
            }
        }
    }
}