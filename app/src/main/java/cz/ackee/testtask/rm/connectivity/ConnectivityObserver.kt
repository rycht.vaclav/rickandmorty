package cz.ackee.testtask.rm.connectivity

import cz.ackee.testtask.rm.Constants
import kotlinx.coroutines.flow.Flow


/*
* Created by Václav Rychtecký on 01/22/2024
*/
interface ConnectivityObserver {

    fun observe(): Flow<Status>

    enum class Status(val text: String) {
        AVAILABLE(Constants.CONNECTION_AVAILABLE),
        LOST(Constants.CONNECTION_LOST),
        UNAVAILABLE(Constants.CONNECTION_UNAVAILABLE)
    }
}