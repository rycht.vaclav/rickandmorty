package cz.ackee.testtask.rm.ui.components.item

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette

/*
* Created by Václav Rychtecký on 01/21/2024
*/
@Composable
fun CharacterDetailItem(modifier: Modifier = Modifier, title: String, text: String) {
    Column(modifier = modifier.padding(top = 16.dp, start = 16.dp, end = 16.dp)) {
        Text(
            modifier = Modifier.fillMaxSize(),
            text = title,
            color = LocalCustomColorsPalette.current.secondaryText
        )
        Text(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 8.dp),
            text = text,
            color = LocalCustomColorsPalette.current.mainText,
            fontWeight = FontWeight.Bold
        )
    }
}