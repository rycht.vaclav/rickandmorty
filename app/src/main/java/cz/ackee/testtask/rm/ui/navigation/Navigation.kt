package cz.ackee.testtask.rm.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.navArgument
import cz.ackee.testtask.rm.ui.screens.CharacterDetail
import cz.ackee.testtask.rm.ui.screens.Characters
import cz.ackee.testtask.rm.ui.screens.Favorite
import cz.ackee.testtask.rm.ui.screens.Search
import cz.ackee.testtask.rm.viewmodels.CharacterDetailViewModel
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.compose.koinViewModel
import org.koin.core.parameter.parametersOf

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@Composable
fun Navigation(navController: NavHostController) {

    val viewModelStoreOwner = checkNotNull(LocalViewModelStoreOwner.current) {
        "No ViewModelStoreOwner was provided via LocalViewModelStoreOwner"
    }

    NavHost(navController = navController, startDestination = Screen.Characters.route) {
        composable(route = Screen.Characters.route) {
            CompositionLocalProvider(LocalViewModelStoreOwner provides viewModelStoreOwner) {
                Characters(navController = navController, viewModel = koinViewModel())
            }
        }
        composable(route = Screen.Favorite.route) {
            CompositionLocalProvider(LocalViewModelStoreOwner provides viewModelStoreOwner) {
                Favorite(navController = navController, viewModel = koinViewModel())
            }
        }
        composable(route = Screen.CharactersDetail.route + "/{characterId}", arguments = listOf(
            navArgument("characterId") { type = NavType.IntType }
        )) { backStackEntry ->
            val characterId = backStackEntry.arguments?.getInt("characterId") ?: 0
            val characterDetailViewModel: CharacterDetailViewModel = getViewModel {
                parametersOf(characterId)
            }
            CompositionLocalProvider(LocalViewModelStoreOwner provides viewModelStoreOwner) {
                CharacterDetail(navController = navController, viewModel = characterDetailViewModel)
            }
        }
        composable(route = Screen.Search.route) {
            CompositionLocalProvider(LocalViewModelStoreOwner provides viewModelStoreOwner) {
                Search(navController = navController, viewModel = koinViewModel())
            }
        }
    }
}

@Composable
fun getCurrentRoute(navController: NavHostController): String? {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    return navBackStackEntry?.destination?.route
}