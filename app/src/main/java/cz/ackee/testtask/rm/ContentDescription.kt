package cz.ackee.testtask.rm

/*
* Created by Václav Rychtecký on 01/19/2024
*/
object ContentDescription {
    const val ARROW_BACK = "Arrow back"
    const val SAD_MORTY = "Sad morty picture"
    const val SEARCH = "Search"
    const val CLEAR = "Clear"
    const val CHAR_DESCRIPTION = "Characters icon"
    const val FAV_DESCRIPTION = "Favorite icon"
}