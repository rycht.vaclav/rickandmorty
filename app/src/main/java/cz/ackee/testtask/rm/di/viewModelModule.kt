package cz.ackee.testtask.rm.di

import cz.ackee.testtask.rm.viewmodels.CharacterDetailViewModel
import cz.ackee.testtask.rm.viewmodels.CharacterListViewModel
import cz.ackee.testtask.rm.viewmodels.FavoriteViewModel
import cz.ackee.testtask.rm.viewmodels.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

/*
* Created by Václav Rychtecký on 01/22/2024
*/
val viewModelModule: Module = module {
    viewModel { CharacterListViewModel(get()) }
    viewModel { FavoriteViewModel(get()) }
    viewModel { (characterId: Int) ->
        CharacterDetailViewModel(
            get(),
            get(),
            characterId = characterId
        )
    }
    viewModel { SearchViewModel(get()) }
}