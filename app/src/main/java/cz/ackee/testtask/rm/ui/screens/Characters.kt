package cz.ackee.testtask.rm.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.paging.compose.collectAsLazyPagingItems
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.rm.ui.components.item.CharacterListItem
import cz.ackee.testtask.rm.ui.navigation.Screen
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette
import cz.ackee.testtask.rm.viewmodels.CharacterListViewModel
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Characters(navController: NavController, viewModel: CharacterListViewModel) {

    val list = viewModel.characterList.collectAsLazyPagingItems()

    Timber.d("COUNT: ${list.itemCount}")

    if (list.itemCount == 0) {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(
                modifier = Modifier.size(104.dp),
                color = LocalCustomColorsPalette.current.tertiary
            )
        }
    } else {
        LazyColumn(
            modifier = Modifier
                .background(LocalCustomColorsPalette.current.secondary),
            contentPadding = PaddingValues(top = 8.dp, bottom = 8.dp)
        ) {
            items(count = list.itemCount) {
                val characterItem = list[it]
                characterItem?.let { character ->
                    CharacterListItem(character = character, onClick = {
                        navController.navigate(Screen.CharactersDetail.route + "/${it}")
                    })
                }
            }
        }
    }
}