package cz.ackee.testtask.rm.activities

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.rm.BuildConfig
import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.ContentDescription
import cz.ackee.testtask.rm.R
import cz.ackee.testtask.rm.connectivity.ConnectivityObserver
import cz.ackee.testtask.rm.connectivity.NetworkConnectivityObserver
import cz.ackee.testtask.rm.ui.components.bottombar.BottomNavBar
import cz.ackee.testtask.rm.ui.components.topappbar.CharacterDetailTopAppBar
import cz.ackee.testtask.rm.ui.components.topappbar.FavoriteTopAppBar
import cz.ackee.testtask.rm.ui.components.topappbar.MainTopAppBar
import cz.ackee.testtask.rm.ui.components.topappbar.SearchTopAppBar
import cz.ackee.testtask.rm.ui.navigation.Navigation
import cz.ackee.testtask.rm.ui.navigation.getCurrentRoute
import cz.ackee.testtask.rm.ui.scafolds.MainScaffold
import cz.ackee.testtask.rm.ui.screens.NoInternetScreen
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette
import cz.ackee.testtask.rm.ui.theme.RickAndMortyTheme
import timber.log.Timber

class MainActivity : ComponentActivity() {

    private lateinit var connectivityObserver: ConnectivityObserver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        connectivityObserver = NetworkConnectivityObserver(applicationContext)

        setContent {
            RickAndMortyTheme {

                val navController = rememberNavController()
                val route = getCurrentRoute(navController = navController)
                val internetStatus by connectivityObserver.observe()
                    .collectAsState(initial = ConnectivityObserver.Status.UNAVAILABLE)

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MainScaffold(navController = navController, route = route) {
                        Column(
                            modifier = Modifier
                                .padding(paddingValues = it)
                                .fillMaxSize()
                        ) {
                            when (internetStatus) {
                                ConnectivityObserver.Status.AVAILABLE,
                                ConnectivityObserver.Status.LOST -> {
                                    if (internetStatus == ConnectivityObserver.Status.LOST) {
                                        Toast.makeText(
                                            applicationContext,
                                            internetStatus.text,
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                    Navigation(navController)
                                }

                                ConnectivityObserver.Status.UNAVAILABLE -> {
                                    NoInternetScreen(internetStatus.text)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
