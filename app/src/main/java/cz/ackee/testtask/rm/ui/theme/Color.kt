package cz.ackee.testtask.rm.ui.theme

import androidx.compose.ui.graphics.Color

val primaryLight = Color(0xFFFFFFFF)
val secondaryLight = Color(0xFFF4F4F9)
val tertiaryLight = Color(0xFF0000FF)
val mainTextLight = Color(0xFF000000)
val secondaryTextLight = Color(0xFF8B8B8C)
val dividerLight = Color(0xFFF0F0F0)

val primaryDark = Color(0xFF2E2E2F)
val secondaryDark = Color(0xFF181819)
val tertiaryDark = Color(0xFF9595FE)
val mainTextDark = Color(0xFFFFFFFF)
val secondaryTextDark = Color(0xFF8B8B8C)
val dividerDark = Color(0xFFF0F0F0)
