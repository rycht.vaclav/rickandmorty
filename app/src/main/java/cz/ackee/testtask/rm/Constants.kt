package cz.ackee.testtask.rm

/*
* Created by Václav Rychtecký on 01/19/2024
*/
object Constants {

    //Connectivity observer
    const val CONNECTION_LOST = "Lost connection!"
    const val CONNECTION_AVAILABLE = "Connection available!"
    const val CONNECTION_UNAVAILABLE =
        "No internet connection! \n Please check your internet connection!"

    //Character Detail
    const val NAME = "Name"
    const val STATUS = "Status"
    const val SPECIES = "Species"
    const val TYPE = "Type"
    const val GENDER = "Gender"
    const val ORIGIN = "Origin"
    const val LOCATION = "Location"

    //Navigation
    const val CHAR_SCREEN = "char_screen"
    const val CHAR_DETAIL_SCREEN = "char_detail_screen"
    const val SEARCH_SCREEN = "search_screen"
    const val FAV_SCREEN = "fav_screen"

    //Timber
    const val TIMBER_TAG = "R&M"

    //TopAppBar
    const val CHARACTERS = "Characters"
    const val SEARCH_CHAR = "Search characters"
    const val FAVORITE = "Favorite"

    //Favorites
    const val NO_FAV = "No favorites yet"
}