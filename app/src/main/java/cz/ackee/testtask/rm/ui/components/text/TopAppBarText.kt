package cz.ackee.testtask.rm.ui.components.text

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.FontWeight

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@Composable
fun TopAppBarText(text: String) {
    Text(text = text, fontWeight = FontWeight.Bold)
}