package cz.ackee.testtask.rm.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.ackee.testtask.app_domain.feature.character.GetSingleCharacterUseCase
import cz.ackee.testtask.app_domain.feature.character.UpdateFavoriteCharacterUseCase
import cz.ackee.testtask.app_domain.model.Character
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class CharacterDetailViewModel(
    private val getSingleCharacterUseCase: GetSingleCharacterUseCase,
    private val updateFavoriteCharacterUseCase: UpdateFavoriteCharacterUseCase,
    private val characterId: Int
) :
    ViewModel() {

    private val _detail = MutableStateFlow<Character?>(null)
    val detail = _detail

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val result = getSingleCharacterUseCase(characterId)
            if (result.isSuccess()) {
                val character = result.getOrNull()
                if (character != null) {
                    _detail.emit(character)
                }
            }
        }
    }

    fun updateFavorite(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            updateFavoriteCharacterUseCase(id)
        }
    }
}