package cz.ackee.testtask.rm.ui.navigation

import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.ContentDescription
import cz.ackee.testtask.rm.R

/*
* Created by Václav Rychtecký on 01/19/2024
*/
sealed class Screen(
    val route: String,
    val icon: Int = 0,
    val contentDescription: String = "",
    val label: String = ""
) {
    object Characters :
        Screen(
            Constants.CHAR_SCREEN,
            R.drawable.ic_char,
            ContentDescription.CHAR_DESCRIPTION,
            Constants.CHARACTERS
        )

    object Favorite :
        Screen(
            Constants.FAV_SCREEN,
            R.drawable.ic_favorite,
            ContentDescription.FAV_DESCRIPTION,
            Constants.FAVORITE
        )

    object CharactersDetail : Screen(Constants.CHAR_DETAIL_SCREEN)
    object Search : Screen(Constants.SEARCH_SCREEN)
}
