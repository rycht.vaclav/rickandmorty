package cz.ackee.testtask.rm.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import cz.ackee.testtask.app_domain.feature.character.SearchByNameUseCase
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.NameSearchItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okio.IOException
import timber.log.Timber
import java.net.URLEncoder

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class CharactersByNamePagingSource(
    private val searchByNameUseCase: SearchByNameUseCase,
    private var name: String
) : PagingSource<Int, Character>() {

    override fun getRefreshKey(state: PagingState<Int, Character>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Character> {

        if (name.isEmpty()) {
            return LoadResult.Page(emptyList(), prevKey = null, nextKey = null)
        }

        val pageNumber = params.key ?: 1
        val previousKey = if (pageNumber == 1) null else pageNumber - 1

        val response = searchByNameUseCase(NameSearchItem(page = pageNumber, name = name))

        return if (response.isSuccess()) {
            val body = response.getOrNull()
            if (body != null) {
                val characters = body.results
                LoadResult.Page(
                    data = characters,
                    prevKey = previousKey,
                    nextKey = getPageIndexFromNext(body.info.next)
                )
            } else {
                LoadResult.Error(response.errorOrNull()?.throwable ?: Throwable())
            }
        } else {
            LoadResult.Error(response.errorOrNull()?.throwable ?: Throwable())
        }
    }

    private fun getPageIndexFromNext(next: String?): Int? {

        if (next == null) {
            return null
        }

        val remainder = next.substringAfter("page=")
        val finalIndex = if (remainder.contains('&')) {
            remainder.indexOfFirst { it == '&' }
        } else {
            remainder.length
        }

        return remainder.substring(0, finalIndex).toIntOrNull()
    }
}