package cz.ackee.testtask.rm.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.ui.components.item.CharacterListItem
import cz.ackee.testtask.rm.ui.components.topappbar.FavoriteTopAppBar
import cz.ackee.testtask.rm.ui.navigation.Screen
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette
import cz.ackee.testtask.rm.viewmodels.FavoriteViewModel

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@Composable
fun Favorite(navController: NavController, viewModel: FavoriteViewModel) {

    val favorite = viewModel.favorites.collectAsState().value

    SideEffect {
        viewModel.getFavorites()
    }

    if (favorite.isNotEmpty()) {
        LazyColumn(
            modifier = Modifier
                .background(LocalCustomColorsPalette.current.secondary),
            contentPadding = PaddingValues(top = 8.dp, bottom = 8.dp)
        ) {
            items(count = favorite.size) {
                val characterItem = favorite[it]
                CharacterListItem(character = characterItem, onClick = {
                    navController.navigate(Screen.CharactersDetail.route + "/${characterItem.id}")
                })
            }
        }
    } else {
        Column(modifier = Modifier.fillMaxSize()) {
            Spacer(modifier = Modifier.weight(1f))
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = Constants.NO_FAV,
                color = LocalCustomColorsPalette.current.secondaryText,
                textAlign = TextAlign.Center
            )
            Spacer(modifier = Modifier.weight(1f))
        }
    }
}