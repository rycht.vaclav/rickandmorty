package cz.ackee.testtask.rm.ui.scafolds

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.ui.components.bottombar.BottomNavBar
import cz.ackee.testtask.rm.ui.components.topappbar.FavoriteTopAppBar
import cz.ackee.testtask.rm.ui.components.topappbar.MainTopAppBar
import cz.ackee.testtask.rm.ui.components.topappbar.SearchTopAppBar
import org.koin.androidx.compose.koinViewModel

/*
* Created by Václav Rychtecký on 01/22/2024
*/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScaffold(
    navController: NavHostController,
    route: String?,
    content: @Composable (PaddingValues) -> Unit
) {
    Scaffold(
        topBar = {
            when (route) {
                Constants.CHAR_SCREEN -> {
                    MainTopAppBar(navController = navController)
                }

                Constants.FAV_SCREEN -> {
                    FavoriteTopAppBar()
                }

                /*Constants.CHAR_DETAIL_SCREEN -> {
                    CharacterDetailTopAppBar(
                        navController = navController,
                        detail = detail
                    )
                }*/

                Constants.SEARCH_SCREEN -> {
                    SearchTopAppBar(navController = navController, viewModel = koinViewModel())
                }
            }
        },
        bottomBar = {
            if (route == Constants.CHAR_SCREEN || route == Constants.FAV_SCREEN) {
                BottomNavBar(navController = navController)
            }
        }) {
        content(it)
    }
}