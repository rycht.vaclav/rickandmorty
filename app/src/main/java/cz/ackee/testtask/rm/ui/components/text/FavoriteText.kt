package cz.ackee.testtask.rm.ui.components.text

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.foundation.text.appendInlineContent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.Placeholder
import androidx.compose.ui.text.PlaceholderVerticalAlign
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette

/*
* Created by Václav Rychtecký on 01/21/2024
*/
@Composable
fun FavoriteText(modifier: Modifier, character: Character) {
    val id = "id"
    val nameWithStar = buildAnnotatedString {
        append("${character.name} ")
        if (character.isFavorite) {
            appendInlineContent(id, "[icon]")
        }
    }

    val inlineContent = mapOf(
        Pair(
            id,
            InlineTextContent(
                Placeholder(
                    width = 24.sp,
                    height = 24.sp,
                    placeholderVerticalAlign = PlaceholderVerticalAlign.Center
                )
            ) {
                Icon(
                    Icons.Filled.Star,
                    "",
                    tint = LocalCustomColorsPalette.current.tertiary
                )
            }
        )
    )

    Text(
        modifier = modifier,
        text = nameWithStar,
        fontSize = 20.sp,
        fontWeight = FontWeight.Bold,
        inlineContent = inlineContent,
        color = LocalCustomColorsPalette.current.mainText
    )
}