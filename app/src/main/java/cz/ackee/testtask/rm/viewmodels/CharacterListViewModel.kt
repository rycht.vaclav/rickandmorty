package cz.ackee.testtask.rm.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import cz.ackee.testtask.app_domain.feature.character.GetCharactersResponseUseCase
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.rm.paging.CharacterPagingSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow

/*
* Created by Václav Rychtecký on 01/19/2024
*/
class CharacterListViewModel(private val getCharactersResponseUseCase: GetCharactersResponseUseCase) :
    ViewModel() {

    var characterList: Flow<PagingData<Character>> = emptyFlow()
    private val pager = Pager(config = PagingConfig(20)) {
        CharacterPagingSource(getCharactersResponseUseCase)
    }.flow

    init {
        characterList = pager.cachedIn(viewModelScope)
    }

}