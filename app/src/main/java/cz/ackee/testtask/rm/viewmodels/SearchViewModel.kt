package cz.ackee.testtask.rm.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import cz.ackee.testtask.app_domain.feature.character.SearchByNameUseCase
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.rm.paging.CharactersByNamePagingSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.launch

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class SearchViewModel(private val searchByNameUseCase: SearchByNameUseCase) : ViewModel() {

    private val _currentSearch = MutableStateFlow("")
    val currentSearch = _currentSearch

    private var charactersByNamePagingSource: CharactersByNamePagingSource? = null
    var characterList: Flow<PagingData<Character>> = emptyFlow()
    val pager = Pager(config = PagingConfig(20)) {
        charactersByNamePagingSource =
            CharactersByNamePagingSource(
                searchByNameUseCase = searchByNameUseCase,
                name = currentSearch.value
            )
        charactersByNamePagingSource!!
    }.flow

    init {
        characterList = pager.cachedIn(viewModelScope)
    }

    fun searchCharactersByName(name: String) {
        currentSearch.value = name
        charactersByNamePagingSource?.invalidate()
    }
}