package cz.ackee.testtask.rm.ui.components.topappbar

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp
import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.ui.components.text.TopAppBarText

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FavoriteTopAppBar() {
    Surface(shadowElevation = 12.dp, color = MaterialTheme.colorScheme.primary) {
        TopAppBar(title = { TopAppBarText(text = Constants.FAVORITE) })
    }
}