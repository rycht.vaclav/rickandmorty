package cz.ackee.testtask.rm.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import cz.ackee.testtask.rm.ContentDescription
import cz.ackee.testtask.rm.R
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette

/*
* Created by Václav Rychtecký on 01/24/2024
*/
@Composable
fun NoInternetScreen(statusText: String) {
    Column(modifier = Modifier.fillMaxSize()) {
        Spacer(modifier = Modifier.weight(1f))
        Image(
            modifier = Modifier
                .size(184.dp)
                .clip(CircleShape)
                .align(Alignment.CenterHorizontally),
            painter = painterResource(id = R.drawable.sad_morty),
            contentDescription = ContentDescription.SAD_MORTY,
            contentScale = ContentScale.Crop
        )
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            text = statusText,
            textAlign = TextAlign.Center,
            color = LocalCustomColorsPalette.current.secondaryText
        )
        Spacer(modifier = Modifier.weight(1f))
    }
}