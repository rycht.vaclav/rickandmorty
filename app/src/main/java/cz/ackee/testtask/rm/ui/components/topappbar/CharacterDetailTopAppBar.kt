package cz.ackee.testtask.rm.ui.components.topappbar

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.StarBorder
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.rm.ContentDescription
import cz.ackee.testtask.rm.ui.components.text.TopAppBarText
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette
import cz.ackee.testtask.rm.viewmodels.CharacterDetailViewModel
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CharacterDetailTopAppBar(
    navController: NavController,
    detail: Character?,
    viewModel: CharacterDetailViewModel
) {

    detail?.let { char ->
        Timber.d("CHAR FAVORITE: ${char.isFavorite}")
        var favoriteState by remember { mutableStateOf(char.isFavorite) }

        Surface(shadowElevation = 12.dp, color = LocalCustomColorsPalette.current.primary) {
            TopAppBar(
                title = {
                    TopAppBarText(text = char.name ?: "")
                },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.KeyboardArrowLeft,
                            contentDescription = ContentDescription.ARROW_BACK
                        )
                    }
                },
                actions = {
                    IconButton(onClick = {
                        viewModel.updateFavorite(detail.id ?: 0)
                        favoriteState = !favoriteState
                    }) {
                        Icon(
                            imageVector = if (favoriteState) Icons.Default.Star else Icons.Default.StarBorder,
                            tint = if (favoriteState) LocalCustomColorsPalette.current.tertiary else LocalCustomColorsPalette.current.mainText,
                            contentDescription = ContentDescription.FAV_DESCRIPTION
                        )
                    }
                }
            )
        }
    }
}