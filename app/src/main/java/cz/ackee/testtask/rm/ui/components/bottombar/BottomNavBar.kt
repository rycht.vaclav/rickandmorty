package cz.ackee.testtask.rm.ui.components.bottombar

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalAbsoluteTonalElevation
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.surfaceColorAtElevation
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.compose.currentBackStackEntryAsState
import cz.ackee.testtask.rm.ui.navigation.Screen
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@Composable
fun BottomNavBar(navController: NavController) {
    val screens = listOf(Screen.Characters, Screen.Favorite)
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    NavigationBar(
        modifier = Modifier
            .background(LocalCustomColorsPalette.current.secondary)
            .clip(RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp)),
    ) {
        Spacer(modifier = Modifier.weight(1f))
        screens.forEach { screen ->
            AddItem(
                screen = screen,
                currentDestination = currentDestination,
                navController = navController
            )
        }
        Spacer(modifier = Modifier.weight(1f))
    }
}

@Composable
fun RowScope.AddItem(
    screen: Screen,
    currentDestination: NavDestination?,
    navController: NavController
) {
    NavigationBarItem(
        selected = currentDestination?.hierarchy?.any { it.route == screen.route } == true,
        onClick = { navController.navigate(screen.route) },
        alwaysShowLabel = true,
        icon = {
            Icon(
                modifier = Modifier
                    .size(48.dp)
                    .padding(bottom = 12.dp),
                painter = painterResource(id = screen.icon),
                contentDescription = screen.contentDescription
            )

        },
        colors = NavigationBarItemDefaults.colors(
            selectedIconColor = LocalCustomColorsPalette.current.tertiary,
            indicatorColor = MaterialTheme.colorScheme.surfaceColorAtElevation(
                LocalAbsoluteTonalElevation.current
            )
        ),
        label = {
            Text(
                modifier = Modifier,
                text = screen.label,
                maxLines = 1
            )
        })
}