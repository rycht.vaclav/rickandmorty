package cz.ackee.testtask.rm.ui.components.item

import android.graphics.drawable.Icon
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.foundation.text.appendInlineContent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.Placeholder
import androidx.compose.ui.text.PlaceholderVerticalAlign
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.rm.R
import cz.ackee.testtask.rm.ui.components.text.FavoriteText
import cz.ackee.testtask.rm.ui.theme.CustomColorsPalette
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette

/*
* Created by Václav Rychtecký on 01/20/2024
*/
@Composable
fun CharacterListItem(character: Character, onClick: (Int) -> Unit, isSearchItem: Boolean = false) {

    Row(
        modifier = if (!isSearchItem) {
            Modifier
                .padding(start = 8.dp, end = 8.dp, top = 8.dp)
                .shadow(elevation = 4.dp, shape = RoundedCornerShape(16.dp))
                .background(LocalCustomColorsPalette.current.primary)
                .clip(shape = RoundedCornerShape(16.dp))
                .clickable { onClick(character.id) }
        } else {
            Modifier
                .padding(start = 8.dp, end = 8.dp, top = 8.dp)
                .background(Color.Transparent)
                .clip(shape = RoundedCornerShape(16.dp))
                .clickable { onClick(character.id) }
        },
        verticalAlignment = Alignment.CenterVertically
    ) {
        AsyncImage(
            modifier = Modifier
                .padding(8.dp)
                .size(56.dp)
                .clip(RoundedCornerShape(8.dp)),
            model = character.image,
            contentDescription = character.name,
            placeholder = painterResource(id = R.drawable.latest),
            contentScale = ContentScale.Crop
        )
        Column {
            FavoriteText(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp, bottom = 2.dp, start = 16.dp, end = 16.dp),
                character = character
            )
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 2.dp, bottom = 16.dp, start = 16.dp, end = 16.dp),
                text = character.status,
                color = LocalCustomColorsPalette.current.secondaryText
            )
        }
    }
}
