package cz.ackee.testtask.rm.ui.components.topappbar

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.ContentDescription
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette
import cz.ackee.testtask.rm.viewmodels.SearchViewModel
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchTopAppBar(navController: NavHostController, viewModel: SearchViewModel) {

    var text by remember { mutableStateOf(TextFieldValue(viewModel.currentSearch.value)) }
    var placeHolderState by remember { mutableStateOf(true) }

    LaunchedEffect(text.text) {
        viewModel.searchCharactersByName(text.text)
    }

    Surface(shadowElevation = 12.dp, color = MaterialTheme.colorScheme.primary) {
        TopAppBar(
            title = {
                TextField(value = text, onValueChange = { text = it })
                BasicTextField(
                    interactionSource = remember { MutableInteractionSource() }
                        .also { interactionSource ->
                            LaunchedEffect(interactionSource) {
                                interactionSource.interactions.collect {
                                    if (it is PressInteraction.Press) {
                                        placeHolderState = false
                                    }
                                }
                            }
                        },
                    value = text,
                    onValueChange = {
                        text = it
                        Timber.d("Value changed: $text")
                    },
                    textStyle = LocalTextStyle.current.copy(
                        color = LocalCustomColorsPalette.current.mainText,
                        fontSize = 15.sp
                    ),
                    cursorBrush = SolidColor(LocalCustomColorsPalette.current.mainText),
                    decorationBox = { innerTextField ->
                        Row(
                            modifier = Modifier
                                .padding(16.dp)
                                .fillMaxSize(),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            if (text.text.isEmpty() && placeHolderState) {
                                Text(
                                    text = Constants.SEARCH_CHAR,
                                    fontSize = 15.sp,
                                    color = Color.LightGray
                                )
                            }
                            innerTextField()
                        }
                    })
            },
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(
                        imageVector = Icons.Filled.KeyboardArrowLeft,
                        contentDescription = ContentDescription.ARROW_BACK
                    )
                }
            },
            actions = {
                IconButton(onClick = {
                    text = TextFieldValue("")
                }) {
                    Icon(
                        imageVector = Icons.Filled.Clear,
                        contentDescription = ContentDescription.CLEAR
                    )
                }
            }
        )
    }
}