package cz.ackee.testtask.rm.ui.theme

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

/*
* Created by Václav Rychtecký on 01/21/2024
*/
@Immutable
data class CustomColorsPalette(
    val primary: Color = Color.Unspecified,
    val secondary: Color = Color.Unspecified,
    val tertiary: Color = Color.Unspecified,
    val mainText: Color = Color.Unspecified,
    val secondaryText: Color = Color.Unspecified,
    val divider: Color = Color.Unspecified
)

val darkColorsPalette = CustomColorsPalette(
    primary = primaryDark,
    secondary = secondaryDark,
    tertiary = tertiaryDark,
    mainText = mainTextDark,
    secondaryText = secondaryTextDark,
    divider = dividerDark
)

val lightColorsPalette = CustomColorsPalette(
    primary = primaryLight,
    secondary = secondaryLight,
    tertiary = tertiaryLight,
    mainText = mainTextLight,
    secondaryText = secondaryTextLight,
    divider = dividerLight
)

val LocalCustomColorsPalette = staticCompositionLocalOf { CustomColorsPalette() }