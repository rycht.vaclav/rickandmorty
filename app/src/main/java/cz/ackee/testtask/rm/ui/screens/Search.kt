package cz.ackee.testtask.rm.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.paging.compose.collectAsLazyPagingItems
import cz.ackee.testtask.rm.ui.components.item.CharacterListItem
import cz.ackee.testtask.rm.ui.navigation.Screen
import cz.ackee.testtask.rm.ui.theme.LocalCustomColorsPalette
import cz.ackee.testtask.rm.viewmodels.SearchViewModel
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/21/2024
*/
@Composable
fun Search(navController: NavController, viewModel: SearchViewModel) {
    Column(modifier = Modifier.padding()) {

        val list = viewModel.characterList.collectAsLazyPagingItems()

        LazyColumn(
            modifier = Modifier
                .background(LocalCustomColorsPalette.current.secondary),
            contentPadding = PaddingValues(top = 8.dp, bottom = 8.dp)
        ) {
            items(count = list.itemCount) {
                val characterItem = list[it]
                characterItem?.let { character ->
                    CharacterListItem(character = character, onClick = {
                        navController.navigate(Screen.CharactersDetail.route + "/${it}")
                    }, isSearchItem = true)
                }
            }
        }
    }
}