package cz.ackee.testtask.rm.ui.components.topappbar

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import cz.ackee.testtask.rm.Constants
import cz.ackee.testtask.rm.ContentDescription
import cz.ackee.testtask.rm.ui.components.text.TopAppBarText
import cz.ackee.testtask.rm.ui.navigation.Screen

/*
* Created by Václav Rychtecký on 01/19/2024
*/
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainTopAppBar(navController: NavHostController) {
    Surface(shadowElevation = 12.dp, color = MaterialTheme.colorScheme.primary) {
        TopAppBar(
            modifier = Modifier,
            title = { TopAppBarText(text = Constants.CHARACTERS) },
            actions = {
                IconButton(onClick = { navController.navigate(Screen.Search.route) }) {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = ContentDescription.SEARCH
                    )
                }
            }
        )
    }
}