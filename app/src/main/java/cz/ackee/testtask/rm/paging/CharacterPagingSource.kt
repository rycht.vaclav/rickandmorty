package cz.ackee.testtask.rm.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import cz.ackee.testtask.app_domain.feature.character.GetCharactersResponseUseCase
import cz.ackee.testtask.app_domain.model.Character
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class CharacterPagingSource(private val getCharactersResponseUseCase: GetCharactersResponseUseCase) :
    PagingSource<String, Character>() {
    override fun getRefreshKey(state: PagingState<String, Character>): String? {
        return null
    }

    override suspend fun load(params: LoadParams<String>): LoadResult<String, Character> {
        val nextPageUrl = params.key ?: INITIAL_LOAD_URL
        val response = getCharactersResponseUseCase(nextPageUrl)

        return if (response.isSuccess()) {
            val body = response.getOrNull()
            if (body != null) {
                val characters = body.results
                LoadResult.Page(
                    data = characters ?: listOf(),
                    prevKey = body.info?.prev,
                    nextKey = body.info?.next
                )
            } else {
                LoadResult.Error(response.errorOrNull()?.throwable ?: Throwable())
            }
        } else {
            LoadResult.Error(response.errorOrNull()?.throwable ?: Throwable())
        }
    }

    companion object {
        private const val INITIAL_LOAD_URL = "https://rickandmortyapi.com/api/character"
    }
}