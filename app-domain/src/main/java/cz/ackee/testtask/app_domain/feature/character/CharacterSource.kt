package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.model.NameSearchItem

/*
* Created by Václav Rychtecký on 01/22/2024
*/
interface CharacterSource {
    suspend fun getCharResponse(params: String): Result<CharactersResponse>
    suspend fun getCharByName(params: NameSearchItem): Result<CharactersResponse>
    suspend fun getById(params: Int): Result<Character>
}