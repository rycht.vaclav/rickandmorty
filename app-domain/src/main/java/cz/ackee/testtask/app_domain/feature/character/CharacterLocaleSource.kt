package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.model.Favorite

/*
* Created by Václav Rychtecký on 01/22/2024
*/
interface CharacterLocaleSource {
    fun saveChars(results: List<Character>?)
    fun getSingleCharacter(params: Int): Result<Character>
    suspend fun upsertFavorite(params: Int)
    fun saveChar(character: Character)
    fun getFavorites(): Result<List<Character>>
    fun getSingleFavorite(id: Int): Favorite?
    fun getFavoritesFromDB(): List<Favorite?>
}