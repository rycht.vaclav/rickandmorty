package cz.ackee.testtask.app_domain.model

/*
* Created by Václav Rychtecký on 01/23/2024
*/
data class Favorite(
    val id: Int,
    val isFavorite: Boolean
)