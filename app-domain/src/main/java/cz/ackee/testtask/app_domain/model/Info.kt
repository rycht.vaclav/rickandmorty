package cz.ackee.testtask.app_domain.model

/*
* Created by Václav Rychtecký on 01/22/2024
*/
data class Info(
    val count: Int?,
    val pages: Int?,
    val next: String?,
    val prev: String?
)
