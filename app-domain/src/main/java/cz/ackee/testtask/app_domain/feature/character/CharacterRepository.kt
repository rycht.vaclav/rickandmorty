package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.model.NameSearchItem

/*
* Created by Václav Rychtecký on 01/22/2024
*/
interface CharacterRepository {
    suspend fun getResponse(params: String): Result<CharactersResponse>
    suspend fun getSingleCharacter(params: Int): Result<Character>
    suspend fun updateFavorite(params: Int)
    suspend fun getFavoriteCharacters(): Result<List<Character>>
    suspend fun findByName(params: NameSearchItem): Result<CharactersResponse>
}