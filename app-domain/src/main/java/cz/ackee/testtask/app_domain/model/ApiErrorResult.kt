package cz.ackee.testtask.app_domain.model

import cz.ackee.testtask.app_domain.ErrorResult

/*
* Created by Václav Rychtecký on 01/22/2024
*/
data class ApiErrorResult(
    val code: Int,
    val errorMessage: String? = null,
    val apiThrowable: Throwable? = null
) : ErrorResult(errorMessage, apiThrowable)