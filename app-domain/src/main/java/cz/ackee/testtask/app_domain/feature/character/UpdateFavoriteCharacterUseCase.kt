package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.usecase.UseCase
import cz.ackee.testtask.app_domain.usecase.UseCaseResult

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class UpdateFavoriteCharacterUseCase(private val characterRepository: CharacterRepository): UseCase<Unit, Int>() {
    override suspend fun doWork(params: Int): Unit = characterRepository.updateFavorite(params)
}