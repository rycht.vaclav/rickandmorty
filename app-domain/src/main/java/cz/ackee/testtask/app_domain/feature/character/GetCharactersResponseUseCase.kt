package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.usecase.UseCaseResult

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class GetCharactersResponseUseCase(private val characterRepository: CharacterRepository) :
    UseCaseResult<CharactersResponse, String>() {

    override suspend fun doWork(params: String): Result<CharactersResponse> =
        characterRepository.getResponse(params)
}