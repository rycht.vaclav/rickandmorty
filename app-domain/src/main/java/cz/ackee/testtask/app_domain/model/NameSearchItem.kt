package cz.ackee.testtask.app_domain.model

/*
* Created by Václav Rychtecký on 01/23/2024
*/
data class NameSearchItem(val page: Int, val name: String)