package cz.ackee.testtask.app_domain.model

/*
* Created by Václav Rychtecký on 01/22/2024
*/
data class CharactersResponse(
    val info: Info,
    val results: List<Character>
)