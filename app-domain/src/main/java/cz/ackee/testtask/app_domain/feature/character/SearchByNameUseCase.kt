package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.model.NameSearchItem
import cz.ackee.testtask.app_domain.usecase.UseCaseResult

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class SearchByNameUseCase(private val characterRepository: CharacterRepository) :
    UseCaseResult<CharactersResponse, NameSearchItem>() {
    override suspend fun doWork(params: NameSearchItem): Result<CharactersResponse> =
        characterRepository.findByName(params)
}