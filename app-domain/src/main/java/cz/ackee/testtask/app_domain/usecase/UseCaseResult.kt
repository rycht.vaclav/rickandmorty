package cz.ackee.testtask.app_domain.usecase

import cz.ackee.testtask.app_domain.Result

/*
* Created by Václav Rychtecký on 01/22/2024
*/
abstract class UseCaseResult<out T : Any, in Params> : UseCase<Result<T>, Params>()