package cz.ackee.testtask.app_domain.model

/*
* Created by Václav Rychtecký on 01/21/2024
*/
data class Character(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val origin: String,
    val location: String,
    val image: String,
    var isFavorite: Boolean = false
)
