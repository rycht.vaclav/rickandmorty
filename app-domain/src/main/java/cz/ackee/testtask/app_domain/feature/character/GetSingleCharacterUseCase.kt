package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.usecase.UseCaseResult

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class GetSingleCharacterUseCase(private val characterRepository: CharacterRepository) :
    UseCaseResult<Character, Int>() {
    override suspend fun doWork(params: Int): Result<Character> =
        characterRepository.getSingleCharacter(params)
}