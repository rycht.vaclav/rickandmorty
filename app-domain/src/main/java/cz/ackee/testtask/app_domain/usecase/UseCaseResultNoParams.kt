package cz.ackee.testtask.app_domain.usecase

import cz.ackee.testtask.app_domain.Result

/*
* Created by Václav Rychtecký on 01/22/2024
*/
abstract class UseCaseResultNoParams<out T : Any> : UseCase<Result<T>, Unit>() {
    suspend operator fun invoke() = super.invoke(Unit)
}