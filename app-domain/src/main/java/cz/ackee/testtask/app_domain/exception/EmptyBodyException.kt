package cz.ackee.testtask.app_domain.exception

import java.io.IOException

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class EmptyBodyException : IOException("Response with empty body")