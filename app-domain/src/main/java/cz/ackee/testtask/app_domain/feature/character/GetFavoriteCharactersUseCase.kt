package cz.ackee.testtask.app_domain.feature.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.usecase.UseCaseResultNoParams

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class GetFavoriteCharactersUseCase(private val characterRepository: CharacterRepository) :
    UseCaseResultNoParams<List<Character>>() {
    override suspend fun doWork(params: Unit): Result<List<Character>> = characterRepository.getFavoriteCharacters()
}