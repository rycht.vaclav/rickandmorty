package cz.ackee.testtask.app_data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.ackee.testtask.app_data.features.character.model.dao.FavoriteDao
import cz.ackee.testtask.app_data.features.character.model.entity.FavoriteEntity

/*
* Created by Václav Rychtecký on 01/23/2024
*/
@Database(entities = [FavoriteEntity::class], version = 1, exportSchema = false)
abstract class RickAndMortyDB : RoomDatabase() {
    abstract val favoriteDao: FavoriteDao
}