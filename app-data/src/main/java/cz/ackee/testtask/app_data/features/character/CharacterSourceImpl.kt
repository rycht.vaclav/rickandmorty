package cz.ackee.testtask.app_data.features.character

import cz.ackee.testtask.app_data.features.character.mapper.CharacterMapper
import cz.ackee.testtask.app_data.network.RickAndMortyApi
import cz.ackee.testtask.app_domain.ErrorResult
import cz.ackee.testtask.app_domain.feature.character.CharacterSource
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.model.ApiErrorResult
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.NameSearchItem
import retrofit2.HttpException

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class CharacterSourceImpl(private val rickAndMortyApi: RickAndMortyApi) : CharacterSource {

    override suspend fun getCharResponse(params: String): Result<CharactersResponse> {
        return try {
            val response = rickAndMortyApi.getCharacters(params)
            Result.Success(data = CharacterMapper.mapToCharactersResponseFromDto(response))
        } catch (e: HttpException) {
            Result.Error(
                ApiErrorResult(
                    code = e.code(),
                    errorMessage = e.message
                )
            )
        }
    }

    override suspend fun getCharByName(params: NameSearchItem): Result<CharactersResponse> {
        return try {
            val response =
                rickAndMortyApi.getCharactersByName(page = params.page, name = params.name)

            if (response.info != null && response.results != null) {
                Result.Success(data = CharacterMapper.mapToCharactersResponseFromDto(response))
            } else {
                Result.Error(
                    ErrorResult(message = "response is null")
                )
            }

        } catch (e: HttpException) {
            Result.Error(
                ApiErrorResult(
                    code = e.code(),
                    errorMessage = e.message()
                )
            )
        }
    }

    override suspend fun getById(params: Int): Result<Character> {
        return try {
            val response = rickAndMortyApi.getCharacterById(characterId = params)
            Result.Success(data = CharacterMapper.mapToCharacterFromDto(response))
        } catch (e: HttpException) {
            Result.Error(
                ApiErrorResult(
                    code = e.code(),
                    errorMessage = e.message()
                )
            )
        }
    }
}

