package cz.ackee.testtask.app_data.features.character

import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.feature.character.CharacterLocaleSource
import cz.ackee.testtask.app_domain.feature.character.CharacterRepository
import cz.ackee.testtask.app_domain.feature.character.CharacterSource
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.model.NameSearchItem
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class CharacterRepositoryImpl(
    private val characterSource: CharacterSource,
    private val characterLocaleSource: CharacterLocaleSource
) : CharacterRepository {
    override suspend fun getResponse(params: String): Result<CharactersResponse> {

        val result = characterSource.getCharResponse(params)
        val body = result.getOrNull()

        if (result.isSuccess() && body != null && body.results.isNotEmpty()) {
            characterLocaleSource.saveChars(body.results)
        }

        return result
    }

    override suspend fun getSingleCharacter(params: Int): Result<Character> {

        val localSourceResponse = characterLocaleSource.getSingleCharacter(params)

        return if (localSourceResponse.getOrNull() != null) {
            localSourceResponse
        } else {
            val character = characterSource.getById(params)
            val characterData = character.getOrNull()

            if (character.isSuccess() && characterData != null) {
                characterLocaleSource.saveChar(characterData)
            }

            character
        }
    }

    override suspend fun updateFavorite(params: Int) {
        characterLocaleSource.upsertFavorite(params)
    }

    override suspend fun getFavoriteCharacters(): Result<List<Character>> {

        Timber.d("GetFavorites")

        val favorites = characterLocaleSource.getFavorites()
        val favoritesData = favorites.getOrNull()
        val persistFavorites = characterLocaleSource.getFavoritesFromDB()

        if (favorites.isSuccess() && favoritesData != null){
            persistFavorites.forEach { favorite ->
                favorite?.let {
                    val character = favoritesData.find { it.id == favorite.id }
                    if (character == null) {
                        val newChar = getSingleCharacter(favorite.id)
                        val newCharData = newChar.getOrNull()
                        if (newChar.isSuccess() && newCharData != null) {
                            characterLocaleSource.saveChar(newCharData)
                        }
                    }
                }
            }
        }

        return characterLocaleSource.getFavorites()
    }

    override suspend fun findByName(params: NameSearchItem): Result<CharactersResponse> {
        return characterSource.getCharByName(params)
    }
}