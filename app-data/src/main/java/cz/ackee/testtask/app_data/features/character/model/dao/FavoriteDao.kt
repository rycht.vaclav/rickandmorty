package cz.ackee.testtask.app_data.features.character.model.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import cz.ackee.testtask.app_data.Constants
import cz.ackee.testtask.app_data.features.character.model.entity.FavoriteEntity

/*
* Created by Václav Rychtecký on 01/23/2024
*/
@Dao
interface FavoriteDao {

    @Upsert
    suspend fun saveAsFavorite(favoriteEntity: FavoriteEntity)

    @Query("SELECT * FROM ${Constants.FAVORITE_TABLE} WHERE id LIKE :id")
    fun getFavorite(id: Int): FavoriteEntity

    @Query("SELECT * FROM ${Constants.FAVORITE_TABLE}")
    fun getAllFavorites(): List<FavoriteEntity>
}