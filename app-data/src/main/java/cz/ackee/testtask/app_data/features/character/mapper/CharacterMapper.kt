package cz.ackee.testtask.app_data.features.character.mapper

import cz.ackee.testtask.app_data.features.character.model.dto.CharacterDto
import cz.ackee.testtask.app_data.features.character.model.dto.CharactersResponseDto
import cz.ackee.testtask.app_data.features.character.model.dto.InfoDto
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.model.Info

/*
* Created by Václav Rychtecký on 01/22/2024
*/
object CharacterMapper {
    fun mapToCharactersResponseFromDto(charactersResponseDto: CharactersResponseDto): CharactersResponse {
        return CharactersResponse(
            info = mapToInfoFromDto(charactersResponseDto.info),
            results = charactersResponseDto.results.map { mapToCharacterFromDto(it) }
        )
    }

    fun mapToInfoFromDto(infoDto: InfoDto): Info {
        return Info(
            count = infoDto.count,
            pages = infoDto.pages,
            next = infoDto.next,
            prev = infoDto.prev
        )
    }

    fun mapToCharacterFromDto(characterDto: CharacterDto): Character {
        return Character(
            id = characterDto.id,
            name = characterDto.name,
            status = characterDto.status,
            species = characterDto.species,
            type = if (characterDto.type.isNullOrBlank()) {
                "-"
            } else characterDto.type,
            gender = characterDto.gender,
            origin = characterDto.origin.name,
            location = characterDto.location.name,
            image = characterDto.image
        )
    }
}