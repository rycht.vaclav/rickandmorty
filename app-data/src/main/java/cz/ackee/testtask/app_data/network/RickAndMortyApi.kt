package cz.ackee.testtask.app_data.network

import cz.ackee.testtask.app_data.features.character.model.dto.CharacterDto
import cz.ackee.testtask.app_data.features.character.model.dto.CharactersResponseDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

/*
* Created by Václav Rychtecký on 01/22/2024
*/
interface RickAndMortyApi {

    @GET
    suspend fun getCharacters(@Url url: String): CharactersResponseDto

    @GET("character/{character-id}")
    suspend fun getCharacterById(@Path("character-id") characterId: Int): CharacterDto

    @GET("character")
    suspend fun getCharactersByName(
        @Query("page") page: Int,
        @Query("name") name: String
    ): CharactersResponseDto
}