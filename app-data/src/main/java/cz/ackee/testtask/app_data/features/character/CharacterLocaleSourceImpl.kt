package cz.ackee.testtask.app_data.features.character

import cz.ackee.testtask.app_data.features.character.mapper.FavoriteMapper
import cz.ackee.testtask.app_data.features.character.model.dao.FavoriteDao
import cz.ackee.testtask.app_data.features.character.model.entity.FavoriteEntity
import cz.ackee.testtask.app_domain.ErrorResult
import cz.ackee.testtask.app_domain.Result
import cz.ackee.testtask.app_domain.feature.character.CharacterLocaleSource
import cz.ackee.testtask.app_domain.model.Character
import cz.ackee.testtask.app_domain.model.CharactersResponse
import cz.ackee.testtask.app_domain.model.Favorite
import timber.log.Timber

/*
* Created by Václav Rychtecký on 01/22/2024
*/
class CharacterLocaleSourceImpl(private val favoriteDao: FavoriteDao) : CharacterLocaleSource {

    private var characters: MutableList<Character> = mutableListOf()

    override fun saveChars(results: List<Character>?) {
        Timber.d("saveChars")
        results?.forEach {
            saveChar(it)
        }
    }

    override fun saveChar(character: Character) {
        Timber.d("Character: ${character.name}")
        if (!characters.contains(character)) {
            val favorite = getSingleFavorite(character.id)
            if (favorite != null) {
                character.isFavorite = favorite.isFavorite
            }
            characters.add(character)
        }
    }

    override fun getSingleFavorite(id: Int): Favorite? {
        return FavoriteMapper.mapFavoriteFromEntity(favoriteDao.getFavorite(id))
    }

    override fun getSingleCharacter(params: Int): Result<Character> {

        val character = characters.find { it.id == params }

        return if (character != null) {
            Result.Success(data = character)
        } else {
            Result.Error(error = ErrorResult("Cannot found character"))
        }
    }

    override suspend fun upsertFavorite(params: Int) {

        val entity = favoriteDao.getFavorite(params)
        Timber.d("Entity: $entity")
        if (entity != null) {
            favoriteDao.saveAsFavorite(FavoriteEntity(id = params, isFavorite = !entity.isFavorite))
        } else {
            favoriteDao.saveAsFavorite(FavoriteEntity(id = params, isFavorite = true))
        }

        val char = characters.find { it.id == params }
        if (char != null) {
            characters.find { it.id == params }?.isFavorite = !char.isFavorite
        }
    }

    override fun getFavoritesFromDB(): List<Favorite?> {
        return FavoriteMapper.mapFavoritesFromEntity(favoriteDao.getAllFavorites())
    }

    override fun getFavorites(): Result<List<Character>> {
        return Result.Success(characters.filter { it.isFavorite })
    }
}