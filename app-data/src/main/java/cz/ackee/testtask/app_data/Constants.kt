package cz.ackee.testtask.app_data

/*
* Created by Václav Rychtecký on 01/23/2024
*/
object Constants {
    const val RICK_AND_MORTY_DATABASE = "RickAndMortyDB"
    const val FAVORITE_TABLE = "favoriteTable"
}