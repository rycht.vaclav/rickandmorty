package cz.ackee.testtask.app_data.di

import android.content.Context
import androidx.room.Room
import cz.ackee.testtask.app_data.Constants
import cz.ackee.testtask.app_data.db.RickAndMortyDB

/*
* Created by Václav Rychtecký on 01/23/2024
*/
fun provideDatabase(context: Context) =
    Room.databaseBuilder(context, RickAndMortyDB::class.java, Constants.RICK_AND_MORTY_DATABASE)
        .allowMainThreadQueries()
        .fallbackToDestructiveMigration().build()

fun provideFavoriteDao(db: RickAndMortyDB) = db.favoriteDao