package cz.ackee.testtask.app_data.features.character.model.dto

/*
* Created by Václav Rychtecký on 01/22/2024
*/
data class InfoDto(
    val count: Int,
    val pages: Int,
    val next: String?,
    val prev: String?
)
