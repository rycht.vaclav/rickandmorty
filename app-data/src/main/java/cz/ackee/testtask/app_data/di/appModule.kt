package cz.ackee.testtask.app_data.di

import cz.ackee.testtask.app_data.features.character.CharacterLocaleSourceImpl
import cz.ackee.testtask.app_data.features.character.CharacterRepositoryImpl
import cz.ackee.testtask.app_data.features.character.CharacterSourceImpl
import cz.ackee.testtask.app_domain.feature.character.CharacterLocaleSource
import cz.ackee.testtask.app_domain.feature.character.CharacterRepository
import cz.ackee.testtask.app_domain.feature.character.CharacterSource
import cz.ackee.testtask.app_domain.feature.character.GetCharactersResponseUseCase
import cz.ackee.testtask.app_domain.feature.character.GetFavoriteCharactersUseCase
import cz.ackee.testtask.app_domain.feature.character.GetSingleCharacterUseCase
import cz.ackee.testtask.app_domain.feature.character.SearchByNameUseCase
import cz.ackee.testtask.app_domain.feature.character.UpdateFavoriteCharacterUseCase
import org.koin.dsl.module

/*
* Created by Václav Rychtecký on 01/22/2024
*/
val appModule = module {

    single { GetCharactersResponseUseCase(get()) }
    single { GetSingleCharacterUseCase(get()) }
    single { UpdateFavoriteCharacterUseCase(get()) }
    single { GetFavoriteCharactersUseCase(get()) }
    single { SearchByNameUseCase(get()) }

    single<CharacterSource> { CharacterSourceImpl(get()) }
    single<CharacterLocaleSource> { CharacterLocaleSourceImpl(get()) }
    single<CharacterRepository> { CharacterRepositoryImpl(get(), get()) }
}