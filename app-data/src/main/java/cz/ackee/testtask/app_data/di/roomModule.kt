package cz.ackee.testtask.app_data.di

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/*
* Created by Václav Rychtecký on 01/23/2024
*/
val roomModule = module {
    single { provideDatabase(androidContext()) }
    single { provideFavoriteDao(get()) }
}