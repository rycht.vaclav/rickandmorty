package cz.ackee.testtask.app_data.features.character.model.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import cz.ackee.testtask.app_data.Constants

/*
* Created by Václav Rychtecký on 01/23/2024
*/
@Entity(tableName = Constants.FAVORITE_TABLE)
class FavoriteEntity(
    @PrimaryKey
    val id: Int,
    val isFavorite: Boolean
)