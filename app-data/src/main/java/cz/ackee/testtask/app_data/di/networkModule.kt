package cz.ackee.testtask.app_data.di

import cz.ackee.testtask.app_data.network.RickAndMortyApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*
* Created by Václav Rychtecký on 01/22/2024
*/
val networkModule = module {
    factory { provideOkHttpClient() }
    factory { provideRickAndMortyApi(get()) }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl("https://rickandmortyapi.com/api/")
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    val interceptor = HttpLoggingInterceptor()
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    return OkHttpClient().newBuilder().addInterceptor(interceptor).build()
}

fun provideRickAndMortyApi(retrofit: Retrofit): RickAndMortyApi =
    retrofit.create(RickAndMortyApi::class.java)
