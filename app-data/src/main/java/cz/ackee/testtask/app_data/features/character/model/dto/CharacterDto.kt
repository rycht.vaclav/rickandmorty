package cz.ackee.testtask.app_data.features.character.model.dto

/*
* Created by Václav Rychtecký on 01/22/2024
*/
data class CharacterDto(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String?,
    val gender: String,
    val origin: LocationDto,
    val location: LocationDto,
    val image: String
)