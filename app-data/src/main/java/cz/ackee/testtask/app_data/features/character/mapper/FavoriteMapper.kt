package cz.ackee.testtask.app_data.features.character.mapper

import cz.ackee.testtask.app_data.features.character.model.entity.FavoriteEntity
import cz.ackee.testtask.app_domain.model.Favorite

/*
* Created by Václav Rychtecký on 01/23/2024
*/
object FavoriteMapper {

    fun mapFavoritesFromEntity(list: List<FavoriteEntity>): List<Favorite?> {
        return list.map { mapFavoriteFromEntity(it) }
    }

    fun mapFavoriteFromEntity(favoriteEntity: FavoriteEntity?): Favorite? {
        return if (favoriteEntity == null){
            null
        } else {
            Favorite(id = favoriteEntity.id, isFavorite = favoriteEntity.isFavorite)
        }
    }
}