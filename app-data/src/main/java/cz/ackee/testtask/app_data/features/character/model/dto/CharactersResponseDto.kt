package cz.ackee.testtask.app_data.features.character.model.dto

/*
* Created by Václav Rychtecký on 01/22/2024
*/
data class CharactersResponseDto(
    val info: InfoDto,
    val results: List<CharacterDto>
)